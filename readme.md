# Timeless Readings

## WebDev
* [Ruby on Rails Guide](http://guides.rubyonrails.org/)
* [7 principles of Rich Web Applications](http://rauchg.com/2014/7-principles-of-rich-web-applications/#server-rendered-pages-are-not-optional)
* [15 TDD Steps to create Rails application](http://andrzejonsoftware.blogspot.com/2007/05/15-tdd-steps-to-create-rails.html)
* [How We Test Rails Applications](https://robots.thoughtbot.com/how-we-test-rails-applications)

## Development Process
* [A Brief Introduction to Scrum](https://www.atlassian.com/agile/scrum)

## Data Science
* [The Open Source Data Science Masters](http://datasciencemasters.org/)
